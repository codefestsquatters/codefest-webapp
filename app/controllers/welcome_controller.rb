class WelcomeController < ApplicationController
  def index

  end

  def create
    @@par = params
    if params[:leisure]
      @leisure = Leisure.new(collection_params)

      @leisure.save
      redirect_to welcome_path(@leisure.id)
    elsif params[:sport]
      @sport = Sport.new(collection_params)

      @sport.save
      redirect_to activity_exercise_level_path
    end
  end

  def show
    if @@par[:leisure]
      @leisure = Leisure.find(params[:id])
    end
  end

  private

  def collection_params

    if params[:leisure]
      params.require(:leisure).permit(:tv_time, :pc_time)
    elsif params[:sport]
      params.require(:sport).permit(:sport_name, :time)
    end
  end

end
