class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :nav

  helper_method :current_user
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def nav
    @activity = 'Activity'
    @activity_sub = ['TV & Computer','Exercise Level']
    @activity_urls = [activity_tv_computer_path, activity_exercise_level_path ]
    @wellness = 'Wellness'
    @wellness_sub = ['Sleep', 'Allergies', 'Diabetes']
    @wellness_urls = [wellness_sleep_path, wellness_allergies_path, wellness_diabetes_path]
    @nutrition = 'Nutrition'
    @nutrition_sub = ['Calorie Counter', 'Weight']
    @nutrition_urls = [nutrition_calorie_counter_path, nutrition_weight_path]
  end


end
