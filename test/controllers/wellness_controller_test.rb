require 'test_helper'

class WellnessControllerTest < ActionController::TestCase
  test "should get sleep" do
    get :sleep
    assert_response :success
  end

  test "should get allergies" do
    get :allergies
    assert_response :success
  end

  test "should get diabetes" do
    get :diabetes
    assert_response :success
  end

end
