require 'test_helper'

class ActivityControllerTest < ActionController::TestCase
  test "should get tv_computer" do
    get :tv_computer
    assert_response :success
  end

  test "should get exercise_level" do
    get :exercise_level
    assert_response :success
  end

end
