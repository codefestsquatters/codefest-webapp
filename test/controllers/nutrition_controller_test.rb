require 'test_helper'

class NutritionControllerTest < ActionController::TestCase
  test "should get calorie_counter" do
    get :calorie_counter
    assert_response :success
  end

  test "should get weight" do
    get :weight
    assert_response :success
  end

end
