# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

for i in 1..5
Sport.create(sport_name: "Soccer", time: 30 - i)
end
for i in 1..15
Sport.create(sport_name: "Weight Lift", time: 60 - 1.3 * i)
end
for i in 1..5
Sport.create(sport_name: "BasketBall", time: 30 + i * 1.4)
end

for i in 1..10
Leisure.create(tv_time: 45 + i * 2, pc_time: 30 + i * 1.4)
end

for i in 1..10
Leisure.create(tv_time: 45 - i * 2, pc_time: 30 + i * 1.9)
end