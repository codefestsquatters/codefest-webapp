class CreateSports < ActiveRecord::Migration
  def change
    create_table :sports do |t|
      t.string :sport_name
      t.float :time

      t.timestamps
    end
  end
end
