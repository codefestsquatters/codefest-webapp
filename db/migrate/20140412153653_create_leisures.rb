class CreateLeisures < ActiveRecord::Migration
  def change
    create_table :leisures do |t|
      t.string :tv_time
      t.string :float
      t.string :pc_time
      t.string :float

      t.timestamps
    end
  end
end
